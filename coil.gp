#!/usr/bin/gnuplot -p

set terminal epslatex size 8cm,8cm standalone header \
"\\usepackage{siunitx}\n \
\\usepackage{times}\n \
\\usepackage{mathptmx}\n"

set xlabel "Position, $x / \\si\{\\meter\}$"
set ylabel "Position, $y / \\si\{\\meter\}$"

# displays the x, y and z axis
set xzeroaxis linewidth 0.5
set yzeroaxis linewidth 0.5
set zzeroaxis linewidth 0.5

# displays the x, y and z grid
set grid xtics linecolor rgb "#888888" linewidth 0.2 linetype 9
set grid ytics linecolor rgb "#888888" linewidth 0.2 linetype 9
set grid ztics linecolor rgb "#888888" linewidth 0.2 linetype 9

# moves the x, y grid to 0
set xyplane at -1.2

# sets the axis range
set xrange [-1.01:1.01]
set yrange [-1.01:1.01]
set zrange [-1.01:1.01]

set xtics -1,1,1
set ytics -1,1,1
set ztics -1,1,1

# moves the key out of the graph
set key outside vertical bottom right

# hides the key
set key off

set output 'figures/coil_Bx.tex'
set view 70,20
set view equal xyz
set xtics offset 0,-0.1
set xlabel "Position, $x / \\si\{\\meter\}$" offset 0,-0.1
set ylabel "Position, $y / \\si\{\\meter\}$" offset -0.2,-0.4
set zlabel "Position, $z / \\si\{\\meter\}$" rotate parallel offset 3.0,0
splot "coil_Bx.dat" with lines linetype rgb "black"

set output 'figures/coil_By.tex'
set view 70,20
set view equal xyz
set xtics offset 0,-0.1
set xlabel "Position, $x / \\si\{\\meter\}$" offset 0,-0.1
set ylabel "Position, $y / \\si\{\\meter\}$" offset -0.2,-0.4
set zlabel "Position, $z / \\si\{\\meter\}$" rotate parallel offset 3.0,0
splot "coil_By.dat" with lines linetype rgb "black"

set output 'figures/coil_Bz.tex'
set view 70,20
set view equal xyz
set xtics offset 0,-0.1
set xlabel "Position, $x / \\si\{\\meter\}$" offset 0,-0.1
set ylabel "Position, $y / \\si\{\\meter\}$" offset -0.2,-0.4
set zlabel "Position, $z / \\si\{\\meter\}$" rotate parallel offset 3.0,0
splot "coil_Bz.dat" with lines linetype rgb "black"

set output 'figures/coil_dBxDy.tex'
set view 70,20
set view equal xyz
set xtics offset 0,-0.1
set xlabel "Position, $x / \\si\{\\meter\}$" offset 0,-0.1
set ylabel "Position, $y / \\si\{\\meter\}$" offset -0.2,-0.4
set zlabel "Position, $z / \\si\{\\meter\}$" rotate parallel offset 3.0,0
splot "coil_dBxDy.dat" with lines linetype rgb "black"

set output 'figures/coil_dBxDx.tex'
set view 70,20
set view equal xyz
set xtics offset 0,-0.1
set xlabel "Position, $x / \\si\{\\meter\}$" offset 0,-0.1
set ylabel "Position, $y / \\si\{\\meter\}$" offset -0.2,-0.4
set zlabel "Position, $z / \\si\{\\meter\}$" rotate parallel offset 3.0,0
splot "coil_dBxDx.dat" with lines linetype rgb "black"

set output 'figures/coil_dBzDz.tex'
set view 70,20
set view equal xyz
set xtics offset 0,-0.1
set xlabel "Position, $x / \\si\{\\meter\}$" offset 0,-0.1
set ylabel "Position, $y / \\si\{\\meter\}$" offset -0.2,-0.4
set zlabel "Position, $z / \\si\{\\meter\}$" rotate parallel offset 3.0,0
splot "coil_dBzDz.dat" with lines linetype rgb "black"

set output 'figures/coil_dBxDz.tex'
set view 70,20
set view equal xyz
set xtics offset 0,-0.1
set xlabel "Position, $x / \\si\{\\meter\}$" offset 0,-0.1
set ylabel "Position, $y / \\si\{\\meter\}$" offset -0.2,-0.4
set zlabel "Position, $z / \\si\{\\meter\}$" rotate parallel offset 3.0,0
splot "coil_dBxDz.dat" with lines linetype rgb "black"

set output 'figures/coil_dBzDy.tex'
set view 70,20
set view equal xyz
set xtics offset 0,-0.1
set xlabel "Position, $x / \\si\{\\meter\}$" offset 0,-0.1
set ylabel "Position, $y / \\si\{\\meter\}$" offset -0.2,-0.4
set zlabel "Position, $z / \\si\{\\meter\}$" rotate parallel offset 3.0,0
splot "coil_dBzDy.dat" with lines linetype rgb "black"
