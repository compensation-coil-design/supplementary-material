#!/usr/bin/gnuplot -p

load 'moreland.pal'
set terminal epslatex size 8cm,8.5cm standalone header \
"\\usepackage{siunitx}\n \
\\usepackage{times}\n \
\\usepackage{mathptmx}\n"

set style arrow 1 linecolor rgb "black" head filled size 0.05, 20, 60 fixed

set xlabel "Position in $\\si\{\\meter\}$"
set ylabel "Position in $\\si\{\\meter\}$"

set bmargin 1.5
set colorbox horiz user origin .15,.90 size .70,.04
set cbtics offset 0,2.3
set cblabel offset 0,2.0
set cbtics 0.5
set cblabel "Normalized stream function, $\\tilde{S}$"
# set cbrange [-1.0:1.0]
# set format cb "%.1f"


# displays the x and y axis
set xzeroaxis linewidth 0.5
set yzeroaxis linewidth 0.5

# displays the x and y grid
set grid xtics linecolor rgb "#888888" linewidth 0.2 linetype 9
set grid ytics linecolor rgb "#888888" linewidth 0.2 linetype 9

set xtics -2,1,2
set ytics -2,1,2
set ztics -2,1,2

set size square

set key off

set view map

set pm3d interpolate 2,2

set output 'figures/coil-net2d_Bx.tex'
set table 'gridded.dat' # open and use temporary file
set dgrid3d 2,2
splot "sf-net2d_Bx.dat"
unset table             # close temporary file
unset dgrid3d
splot "gridded.dat" with pm3d, "coil-net2d_Bx.dat" u 1:2:(0.0):3:4:(0.0) with vectors arrowstyle 1

set output 'figures/coil-net2d_By.tex'
set table 'gridded.dat' # open and use temporary file
set dgrid3d 2,2
splot "sf-net2d_By.dat"
unset table             # close temporary file
unset dgrid3d
splot "gridded.dat" with pm3d, "coil-net2d_By.dat" u 1:2:(0.0):3:4:(0.0) with vectors arrowstyle 1

set output 'figures/coil-net2d_Bz.tex'
set table 'gridded.dat' # open and use temporary file
set dgrid3d 2,2
splot "sf-net2d_Bz.dat"
unset table             # close temporary file
unset dgrid3d
splot "gridded.dat" with pm3d, "coil-net2d_Bz.dat" u 1:2:(0.0):3:4:(0.0) with vectors arrowstyle 1

set output 'figures/coil-net2d_dBxDy.tex'
set table 'gridded.dat' # open and use temporary file
set dgrid3d 2,2
splot "sf-net2d_dBxDy.dat"
unset table             # close temporary file
unset dgrid3d
splot "gridded.dat" with pm3d, "coil-net2d_dBxDy.dat" u 1:2:(0.0):3:4:(0.0) with vectors arrowstyle 1

set output 'figures/coil-net2d_dBxDx.tex'
set table 'gridded.dat' # open and use temporary file
set dgrid3d 2,2
splot "sf-net2d_dBxDx.dat"
unset table             # close temporary file
unset dgrid3d
splot "gridded.dat" with pm3d, "coil-net2d_dBxDx.dat" u 1:2:(0.0):3:4:(0.0) with vectors arrowstyle 1

set output 'figures/coil-net2d_dBzDz.tex'
set table 'gridded.dat' # open and use temporary file
set dgrid3d 2,2
splot "sf-net2d_dBzDz.dat"
unset table             # close temporary file
unset dgrid3d
splot "gridded.dat" with pm3d, "coil-net2d_dBzDz.dat" u 1:2:(0.0):3:4:(0.0) with vectors arrowstyle 1

set output 'figures/coil-net2d_dBxDz.tex'
set table 'gridded.dat' # open and use temporary file
set dgrid3d 2,2
splot "sf-net2d_dBxDz.dat"
unset table             # close temporary file
unset dgrid3d
splot "gridded.dat" with pm3d, "coil-net2d_dBxDz.dat" u 1:2:(0.0):3:4:(0.0) with vectors arrowstyle 1

set output 'figures/coil-net2d_dBzDy.tex'
set table 'gridded.dat' # open and use temporary file
set dgrid3d 2,2
splot "sf-net2d_dBzDy.dat"
unset table             # close temporary file
unset dgrid3d
splot "gridded.dat" with pm3d, "coil-net2d_dBzDy.dat" u 1:2:(0.0):3:4:(0.0) with vectors arrowstyle 1
